import { useState } from 'react';
import {Hero, About, Education, Experience, More} from './components'
import { CV } from './components/CV/CV';
import './App.scss';

const { hero, education, experience, languages, habilities, volunteer } = CV;

function App() {
const [showEducation, setShowEducation] = useState(true);
  return (

    <div className="App">
      <Hero hero={hero} />
      <About hero={hero} />
      <button
      className='btn'
      onClick={() => setShowEducation(true)}>
        Education
      </button>

      <button
      className='btn'
      onClick={() =>setShowEducation(false)}>
        Experience
      </button>

{      <div>
      {showEducation ? (
          <Education education={education} />
        ) : (
          <Experience experience={experience} />
        )}
      </div>}
      <More
      languages={languages}
      habilities={habilities}
      volunteer={volunteer}
      />
    </div>
  );
}

export default App;
