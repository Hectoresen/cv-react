import { useState } from "react";
import './About.scss';

const About = ({hero}) =>{
    const {aboutMe} = hero;
    console.log(aboutMe)
    const [showAboutMe, setShowAboutMe] = useState(false);

    return <div className="about">
                {showAboutMe
            ?
            (aboutMe.map((element, index) =>{
                return (
                <div className="about-list" key={index}>
                    <p>{element.info}</p>
                </div>
                )
            }))
            :
            <button
                onClick={() => setShowAboutMe(true)}>
                Sobre mi
            </button>
        }
    </div>
}

export default About;